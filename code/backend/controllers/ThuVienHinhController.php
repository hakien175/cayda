<?php

namespace backend\controllers;

use Yii;
use common\models\ThuVienHinh;
use common\models\query\ThuVienHinhQuery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ThuVienHinhController implements the CRUD actions for ThuVienHinh model.
 */
class ThuVienHinhController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ThuVienHinh models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ThuVienHinhQuery();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['id' => SORT_DESC]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ThuVienHinh model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ThuVienHinh model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        
        $model = new ThuVienHinh();

        if ($model->load(Yii::$app->request->post())) {
            // convert base64 to image
             // convert base64 to image
             $data = $model->avatar;
             $fileName = explode('+', $data);
             $strFileName = $fileName[0];
 
             $position = strpos($strFileName, '.', 0);
             $strFileName = substr($strFileName, 0, $position) . strtotime("now") . '.png';
 
             $strEx = explode('base64,', $data);
             $imgFile = base64_decode($strEx[1]);
 
             file_put_contents(Yii::$app->basePath. '/web/upload/thu-vien-hinh-albumn/' . $strFileName, $imgFile);
             $model->avatar = $strFileName;

            $img1 = UploadedFile::getInstances($model,'img1');
           
            $img2 = UploadedFile::getInstances($model, 'img2');
            $img3 = UploadedFile::getInstances($model, 'img3');
            $img4 = UploadedFile::getInstances($model, 'img4');
            $img5 = UploadedFile::getInstances($model, 'img5');
            $img6 = UploadedFile::getInstances($model, 'img6');
            if(!empty($img1)){
                $temp1 = strtotime('now').'-'.$img1[0];
                $path = (Yii::getAlias('@webroot').'/upload/thu-vien-hinh-albumn/'.$temp1);
                $img1[0]->saveAs($path);
                $model->img1= $temp1;
            }
            if(!empty($img2)){
                $temp2 =strtotime('now').'-'.$img2[0];
                $path = (Yii::getAlias('@webroot').'/upload/thu-vien-hinh-albumn/'.$temp2);
                $img2[0]->saveAs($path);
                $model->img2= $temp2;
            }
            if(!empty($img3)){
                $temp3 = strtotime('now').'-'.$img3[0];
                $path = (Yii::getAlias('@webroot').'/upload/thu-vien-hinh-albumn/'.$temp3);
                $img3[0]->saveAs($path);
                $model->img3= $temp3;
            }
            if(!empty($img4)){
                $temp4 = strtotime('now').'-'.$img4[0];
                $path = (Yii::getAlias('@webroot').'/upload/thu-vien-hinh-albumn/'.$temp4);
                $img4[0]->saveAs($path);
                $model->img4= $temp4;
            }
            if(!empty($img5)){
                $temp5 =strtotime('now').'-'.$img5[0];
                $path = (Yii::getAlias('@webroot').'/upload/thu-vien-hinh-albumn/'.$temp5);
                $img5[0]->saveAs($path);
                $model->img5= $temp5;
            }
            if(!empty($img6)){
                $temp6 = strtotime('now').'-'.$img6[0];
                $path = (Yii::getAlias('@webroot').'/upload/thu-vien-hinh-albumn/'.$temp6);
                $img6[0]->saveAs($path);
                $model->img6= $temp6;
            }
            $model->created_at = strtotime('now');
            if($model->save()){
                Yii::$app->session->setFlash('success','Thêm mới albumn ảnh thành công');
                return $this->redirect('index');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ThuVienHinh model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

      
        $model = $this->findModel($id);
        $img1Old = $model->img1;
        $img2Old = $model->img2;
        $img3Old = $model->img3;
        $img4Old = $model->img4;
        $img5Old = $model->img5;
        $img6Old = $model->img6;
        $str = $model->avatar;

        if ($model->load(Yii::$app->request->post())) {
           
            // echo '<pre>';
            // print_r(Yii::$app->request->post());
            // die;
            
            if ($model->avatar == $str) {
                $model->avatar = $str;
            } else {
                $data = $model->avatar;
                if (!empty($model->avatar)) {
                    $fileName = explode('+', $data);
                    $strFileName = $fileName[0];

                    $position = strpos($strFileName, '.', 0);
                    $strFileName = substr($strFileName, 0, $position) . strtotime("now") . '.png';

                    $strEx = explode('base64,', $data);
                    $imgFile = base64_decode($strEx[1]);

                    file_put_contents(Yii::$app->basePath . '/web/upload/articles/' . $strFileName, $imgFile);
                    $model->avatar = $strFileName;
                }
            }
           
            $img1 = UploadedFile::getInstances($model, 'img1');
            if(!empty($img1)){
                $temp1 = strtotime('now').'-'.$img1[0];
                $path = (Yii::getAlias('@webroot').'/upload/thu-vien-hinh-albumn/'.$temp1);
                $img1[0]->saveAs($path);
                $model->img1= $temp1;
            } else {
                $model->img1 = $img1Old;
            }
            $img2 = UploadedFile::getInstances($model, 'img2');
            if(!empty($img2)){
                $temp2 = strtotime('now').'-'.$img2[0];
                $path = (Yii::getAlias('@webroot').'/upload/thu-vien-hinh-albumn/'.$temp2);
                $img2[0]->saveAs($path);
                $model->img2= $temp2;
            } else {
                $model->img2 = $img2Old;
            }
            $img3 = UploadedFile::getInstances($model, 'img3');
            if(!empty($img3)){
                $temp3 = strtotime('now').'-'.$img3[0];
                $path = (Yii::getAlias('@webroot').'/upload/thu-vien-hinh-albumn/'.$temp3);
                $img3[0]->saveAs($path);
                $model->img3= $temp3;
            } else {
                $model->img3 = $img3Old;
            }
            $img4 = UploadedFile::getInstances($model, 'img4');
            if(!empty($img4)){
                $temp4 = strtotime('now').'-'.$img4[0];
                $path = (Yii::getAlias('@webroot').'/upload/thu-vien-hinh-albumn/'.$temp4);
                $img4[0]->saveAs($path);
                $model->img4= $temp4;
            } else {
                $model->img4 = $img4Old;
            }
            $img5 = UploadedFile::getInstances($model, 'img5');
            if(!empty($img5)){
                $temp5 = strtotime('now').'-'.$img5[0];
                $path = (Yii::getAlias('@webroot').'/upload/thu-vien-hinh-albumn/'.$temp5);
                $img5[0]->saveAs($path);
                $model->img5= $temp5;
            } else {
                $model->img5 = $img5Old;
            }
            $img6 = UploadedFile::getInstances($model, 'img6');
            if(!empty($img6)){
                $temp6 = strtotime('now').'-'.$img6[0];
                $path = (Yii::getAlias('@webroot').'/upload/thu-vien-hinh-albumn/'.$temp6);
                $img6[0]->saveAs($path);
                $model->img6= $temp6;
            } else {
                $model->img6 = $img6Old;
            }
            if($model->save()){
                Yii::$app->session->setFlash('success','Cập nhật albumn ảnh thành công');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ThuVienHinh model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ThuVienHinh model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ThuVienHinh the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ThuVienHinh::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
