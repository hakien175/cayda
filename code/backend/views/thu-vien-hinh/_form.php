<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $model common\models\ThuVienHinh */
/* @var $form yii\widgets\ActiveForm */?>

<?php 
if($model->isNewRecord){
    $script = <<< JS
   
    $(".img2").css("display","none");
    $(".img3").css("display","none");
    $(".img4").css("display","none");
    $(".img5").css("display","none");
    $(".img6").css("display","none");
    
    $("#thuvienhinh-img1").change(function() {
        let input = document.getElementById("thuvienhinh-img1");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
             $('#img1-images').attr('src', e.target.result);
             $(".img2").css("display","block");
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
    $("#thuvienhinh-img2").change(function() {
        let input = document.getElementById("thuvienhinh-img2");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#img2-images').attr('src', e.target.result);
            $(".img3").css("display","block");
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
    
    $("#thuvienhinh-img3").change(function() {
        let input = document.getElementById("thuvienhinh-img3");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('#img3-images').attr('src', e.target.result);
                $(".img4").css("display","block");
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
    $("#thuvienhinh-img4").change(function() {
        let input = document.getElementById("thuvienhinh-img4");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('#img4-images').attr('src', e.target.result);
                $(".img5").css("display","block");
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
    $("#thuvienhinh-img5").change(function() {
        let input = document.getElementById("thuvienhinh-img5");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('#img5-images').attr('src', e.target.result);
                $(".img6").css("display","block");
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
    $("#thuvienhinh-img6").change(function() {
        let input = document.getElementById("thuvienhinh-img6");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#img6-images').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
}else{
    $script = <<< JS
        $(".img2").css("display","block");
        $(".img3").css("display","block");
        $(".img4").css("display","block");
        $(".img5").css("display","block");
        $(".img6").css("display","block");
        $("#thuvienhinh-img1").change(function() {
        let input = document.getElementById("thuvienhinh-img1");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
             $('#img1-images').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
    $("#thuvienhinh-img2").change(function() {
        let input = document.getElementById("thuvienhinh-img2");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#img2-images').attr('src', e.target.result);
          
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
    
    $("#thuvienhinh-img3").change(function() {
        let input = document.getElementById("thuvienhinh-img3");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('#img3-images').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
    $("#thuvienhinh-img4").change(function() {
        let input = document.getElementById("thuvienhinh-img4");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('#img4-images').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
    $("#thuvienhinh-img5").change(function() {
        let input = document.getElementById("thuvienhinh-img5");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('#img5-images').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
    $("#thuvienhinh-img6").change(function() {
        let input = document.getElementById("thuvienhinh-img6");
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
            $('#img6-images').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    });
JS;
$this->registerJs($script, \yii\web\View::POS_READY);
}?>

<style>
    .img {
        position: relative;
        width: 210px;
        float: left;
        height: 140px;
        margin: 0px 10px 10px 0;
    }
    .img img {
        width: 200px;
        position: relative;
        height: 140px;
        z-index: 1;
        top: 0;
    }

    .img input {
        width: 210px;
        position: absolute;
        height: 140px;
        top: 0 !important;
        opacity: 0!important;
        z-index: 10;
    }
</style>

<div id="myModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Cắt ảnh</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-10 text-center">
                        <div id="imageArticles" style="width:300px; margin-top:20px"></div>
                    </div>
                    <div class="col-md-2" style="padding-top:20px;">
                        <br />
                        <br />
                        <br/>
                        <button class="btn btn-success crop_imageArticles">Cắt ảnh</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<div class="col-md-12">
    <div class="page-title">                    
        <h2><span class="fa fa-arrow-circle-o-left"></span> <?= $this->title ?></h2>
    </div>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                <div class="img img1">
                    <?= $form->field($model, 'img1')->fileInput()->label(false) ?>
                    <img  src="<?= $model->img1 ? \Yii::$app->request->BaseUrl.'/upload/thu-vien-hinh-albumn/'.$model->img1: 'https://via.placeholder.com/300x200' ?>" id="img1-images"  class="img img-thumbnail" alt="avatar">
                </div>
                <div class="img img2">
                    <?= $form->field($model, 'img2')->fileInput()->label(false) ?>
                    <img  src="<?= $model->img2 ? \Yii::$app->request->BaseUrl.'/upload/thu-vien-hinh-albumn/'.$model->img2: 'https://via.placeholder.com/300x200' ?>" id="img2-images"  class="img img-thumbnail" alt="avatar">
                </div>
                <div class="img img3">
                    <?= $form->field($model, 'img3')->fileInput()->label(false) ?>
                    <img  src="<?= $model->img3 ? \Yii::$app->request->BaseUrl.'/upload/thu-vien-hinh-albumn/'.$model->img3: 'https://via.placeholder.com/300x200' ?>" id="img3-images"  class="img img-thumbnail" alt="avatar">
                </div>
                <div class="img img4">
                    <?= $form->field($model, 'img4')->fileInput()->label(false) ?>
                    <img src="<?= $model->img4 ? \Yii::$app->request->BaseUrl.'/upload/thu-vien-hinh-albumn/'.$model->img4: 'https://via.placeholder.com/300x200' ?>" id="img4-images"  class="img img-thumbnail" alt="avatar">
                </div>
                <div class="img img5">
                    <?= $form->field($model, 'img5')->fileInput()->label(false) ?>
                    <img  src="<?= $model->img5 ? \Yii::$app->request->BaseUrl.'/upload/thu-vien-hinh-albumn/'.$model->img5 : 'https://via.placeholder.com/300x200' ?>" id="img5-images"  class="img img-thumbnail" alt="avatar">
                </div>
                <div class="img img6">
                    <?= $form->field($model, 'img6')->fileInput()->label(false) ?>
                    <img  src="<?= $model->img6 ? \Yii::$app->request->BaseUrl.'/upload/thu-vien-hinh-albumn/'.$model->img6: 'https://via.placeholder.com/300x200' ?>" id="img6-images"  class="img img-thumbnail" alt="avatar">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-body">
                <div id="uploaded">
                    <?= $form->field($model, 'avatar')->hiddenInput(['id'=>'dataUpload']) ?>
                    <?php
                    if(!empty($model->avatar)){ ?>
                        <?= Html::img(\Yii::$app->request->BaseUrl.'/upload/thu-vien-hinh-albumn/'.$model->avatar,['class'=>'avatar img-thumbnail','id'=>'avatarNews'])?>
                    <?php }else{ ?>
                        <img src="https://via.placeholder.com/300x200" id="avatarNews"  class="avatar img-thumbnail" alt="avatar">
                    <?php }
                    ?>
                    <input type="file" id="uploadArticles" />
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                
                <?php
                if ($model->isNewRecord){
                    echo Html::submitButton('Thêm mới', ['class' => 'btn btn-success btn-block']);
                }else{
                    echo Html::submitButton('Cập nhật', ['class' => 'btn btn-primary btn-block']);
                }?>
                <?= Html::a('Quay lại',['index'],['class'=>'btn btn-default btn-block'])?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    <br />
    <br/>
</div>

