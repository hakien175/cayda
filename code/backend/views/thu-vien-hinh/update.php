<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ThuVienHinh */

$this->title = 'Cập nhật thư viện hình';
?>
<div class="thu-vien-hinh-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
