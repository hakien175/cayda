<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ThuVienVideo */

$this->title = 'Cập nhật thư viện Video';
?>

<div class="thu-vien-video-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
