<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\HuongVeQueHuong */

$this->title = 'Thêm mới chuyên mục hướng về quê hương';
?>
<div class="huong-ve-que-huong-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
