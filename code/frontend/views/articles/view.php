<?php
use yii\helpers\Url;
?>
<section class="list_articles_previews">
    <div class="container">
        <div class="row">
            <div class="col-md-8 list_articles_left">
                <div class="detail-article">
                    <h3 class="title_articles">
                        <?= $articles->title ? $articles->title : '' ?>
                    </h3>
                    <hr>
                    <span class="time"><i class="fal fa-clock"></i> <?= date('H:i d/m/Y',$articles->created_at)?></i></span>
                    <br>
                    <br>
                    <p class="description">
                        <?= $articles->description ? $articles->description : '' ?>
                    </p>
                    <hr>
                    <p class="contents">
                        <?= $articles->content ? $articles->content : '' ?>          
                    </p>
                    <i class="float-right bold">
                        Nguồn: <?= ucfirst($articles->user->username) ?>
                    </i>
                    <br>
                    <div class="tags">
                        <b><img src="/theme/images/tags.png" alt="">Từ khóa: </b> 
                        <?php
                        if (!empty($tagsNames)){
                            foreach ($tagsNames as $tagsNames=> $values){?>
                            <a href="<?= Url::to(['list-articles-tags','tag'=>$values])?>" class="tags"><?= $values ?></a>
                        <?php }
                        }
                        ?>
                        
                    </div>
                </div>
                <br>
                <div class="bottom-border">
                </div>
            </div>
            <div class="col-md-4 list_articles_right">
                <div class="title_list_articles">
                    <h3 class="title">Tin nổi bật</h3>
                </div>
                <?php 
                if(!empty($tinNoiBats)){
                    foreach($tinNoiBats as $tinNoiBat){ ?>
                        <div class="item_articles_more_viewed_date">
                            <a href="">
                                <h3 class="title_new"><?=  $tinNoiBat->title ?></h3>
                            </a>
                        <a href="<?= Url::to(['list-articles-cat','code'=>$tinNoiBat->catArtiles->code])?>" class="cat_articles"><?=  $tinNoiBat->catArtiles->name ?></a>
                    </div>
                    <?php }
                }
                ?>
            </div>
        </div>
    </div>
</section>