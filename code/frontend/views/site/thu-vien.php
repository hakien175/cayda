<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Products;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

$this->title = 'Cây đa la tiến';
$upUrl =\yii\helpers\Url::to(['/site/ajax-images']);
$deleteScript = <<< JS
$( ".btnModal" ).on('click', function(e) {
    var thisCtrl = $(this);
    $.ajax({
        type: "POST",
        url: '$upUrl', 
        data: {
            dataId: thisCtrl.attr('dataId')
        },
        success: function(data){
            $('#myModal').modal('show');
            $(".wrap-slider >div").remove();
            $('.wrap-slider').append(data.html);
        }
    });
});
JS;
$this->registerJs($deleteScript, \yii\web\View::POS_READY);
?>

<div class="content container">
    <section class="banner">
        <img src="/theme/images/hinh-anh/banner.png" alt="">
    </section>
    <section class="thu_vien_anh">
        <h3 class="title"><img src="/theme/images/hinh-anh/icon-thu-muc-hinh.png" alt=""> Thư mục hình</h3>
        <!-- Modal -->
        <div id="myModal" class="modal fade modal-slider" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <img src="/theme/images/close-slide.png" />
                        </button>
                        
                    </div>
                    <div class="modal-body">
                        <div class="wrap-slider">
                            
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <p>Hưng Yên và 11 đặc sản ngon vang tiếng gần xa</p>
                            </div>
                            <div class="col-md-3">
                                <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-width="" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
                            </div>
                        </div>
                        
                    </div>

                </div>

            </div>
        </div>
        <?php 
        if(!empty($thuVienHinhs)){
            foreach($thuVienHinhs as $thuVienHinh){ ?>
                <div class="albums">
                    <img src="<?= Yii::getAlias('@fakelink/upload/thu-vien-hinh-albumn/' . $thuVienHinh->avatar) ?>" class="avatar" alt="">
                    <img src="<?= Yii::getAlias('@fakelink/upload/thu-vien-hinh-albumn/' . $thuVienHinh->avatar) ?>" class="sub_avatar" alt="">
                    <a data-toggle="modal" data-target="#myModal" dataId="<?= $thuVienHinh->id?>" class="btnModal"><?= $thuVienHinh->title?></a>
                </div>
            <?php }
            }
        ?>
        
        <nav class="page" aria-label="Page navigation example text-center">
            <?=
                LinkPager::widget([
                    'pagination' => $pages1,
                ]);
            ?>
        </nav>
    </section>
    <section class="thu_vien_video">
        <h3 class="title"><img src="/theme/images/hinh-anh/icon-thu-vien-video.png" alt=""> Thư mục video</h3>
        <?php 
        Pjax::begin();
        if(!empty($thuVienVideos)){
            foreach($thuVienVideos as $thuVienVideo){ ?>
            <div class="videos">
                <iframe width="100%" height="" src="<?= $thuVienVideo->link ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <a class=""><?= $thuVienVideo->title ?> </a>
            </div>
            <?php }
            }
        ?>
        <nav class="page" aria-label="Page navigation example text-center">
            <?=
                LinkPager::widget([
                    'pagination' => $pages,
                ]);
                Pjax::end();
            ?>
        </nav>
    </section>
</div>