
<div class="slider_banners owl-carousel owl-theme">
    <?php 
    if(!empty($images->img1)){ ?>
        <div class="item">
            <div class="banner">
                <a href="#">
                    <img class="ispc" src="<?= Yii::getAlias('@fakelink/upload/thu-vien-hinh-albumn/' . $images->img1) ?>" alt="">
                </a>
            </div>
        </div>
    <?php }
    ?>
     <?php 
    if(!empty($images->img2)){ ?>
        <div class="item">
            <div class="banner">
                <a href="#">
                    <img class="ispc" src="<?= Yii::getAlias('@fakelink/upload/thu-vien-hinh-albumn/' . $images->img2) ?>" alt="">
                </a>
            </div>
        </div>
    <?php }
    ?>
     <?php 
    if(!empty($images->img3)){ ?>
        <div class="item">
            <div class="banner">
                <a href="#">
                    <img class="ispc" src="<?= Yii::getAlias('@fakelink/upload/thu-vien-hinh-albumn/' . $images->img3) ?>" alt="">
                </a>
            </div>
        </div>
    <?php }
    ?>
     <?php 
    if(!empty($images->img4)){ ?>
        <div class="item">
            <div class="banner">
                <a href="#">
                    <img class="ispc" src="<?= Yii::getAlias('@fakelink/upload/thu-vien-hinh-albumn/' . $images->img4) ?>" alt="">
                </a>
            </div>
        </div>
    <?php }
    ?>
     <?php 
    if(!empty($images->img5)){ ?>
        <div class="item">
            <div class="banner">
                <a href="#">
                    <img class="ispc" src="<?= Yii::getAlias('@fakelink/upload/thu-vien-hinh-albumn/' . $images->img5) ?>" alt="">
                </a>
            </div>
        </div>
    <?php }
    ?>
     <?php 
    if(!empty($images->img6)){ ?>
        <div class="item">
            <div class="banner">
                <a href="#">
                    <img class="ispc" src="<?= Yii::getAlias('@fakelink/upload/thu-vien-hinh-albumn/' . $images->img6) ?>" alt="">
                </a>
            </div>
        </div>
    <?php }
    ?>
   
</div>
<a class="previousNews" href="#carouselId" role="button" data-slide="prev">
    <img src="/theme/images/right-slide.jpg" alt="arrow-right.png" class="img-fluid"> </a>
<a class="nextBtnNews" href="#carouselId" role="button" data-slide="next">
    <img src="/theme/images/left-slide.jpg" alt="arrow-right.png" class="img-fluid">
</a>
<script>
    $(document).ready(function(){
        var owlBanner = $(".slider_banners");
            owlBanner.owlCarousel({
                loop: true,
                items: 1,
                margin: 0,
                responsiveClass: true,
                lazyLoad: true,
                nav: false,
                dots: false,
            });
            $(".nextBtnNews").click(function() {
                owlBanner.trigger("next.owl.carousel");
            });
            $(".previousNews").click(function() {
                owlBanner.trigger("prev.owl.carousel");
            });
    });
</script>