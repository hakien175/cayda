<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "thu_vien_hinh".
 *
 * @property int $id
 * @property string $title
 * @property string $avatar
 * * @property string $img1
 * * @property string $img2
 * * @property string $img3
 * * @property string $img4
 * * @property string $img5
 * * @property string $img6
 * 
 */
class ThuVienHinh extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'thu_vien_hinh';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'avatar','created_at'], 'required'],
            [['avatar','img1','img2','img3','img4','img5','img6'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Tiêu đề albumn',
            'avatar' => 'Ảnh tiêu đề',
            'img1' => 'Slide 1',
            'img2' => 'Slide 2',
            'img3' => 'Slide 3',
            'img4' => 'Slide 4',
            'img5' => 'Slide 5',
            'img6' => 'Slide 6',
            
        ];
    }
}
